from django.apps import AppConfig


class SupplierConfig(AppConfig):
    name = 'abc_app.supplier'
