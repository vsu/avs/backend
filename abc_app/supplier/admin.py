from django.contrib import admin
from .models import Address, Supplier

admin.site.register([Address])


class AddressInline(admin.StackedInline):
    model = Address


class SupplierAdmin(admin.ModelAdmin):
    model = Supplier
    inlines = [
        AddressInline,
    ]


admin.site.register(Supplier, SupplierAdmin)
