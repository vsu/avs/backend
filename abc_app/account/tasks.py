from django.core.mail import send_mail
from django.contrib.auth import get_user_model
from settings.celery import app


@app.task
def send_new_password(user_email):
    UserModel = get_user_model()
    user = UserModel.objects.get(email=user_email)
    new_password = UserModel.objects.make_random_password()
    user.set_password(new_password)
    user.save()
    send_mail(
        'Сброс пароля пользователя',
        'Ваш новый пароль {}'.format(new_password),
        'yazubakhin@gmail.com',
        [user.email],
        fail_silently=False,
    )
