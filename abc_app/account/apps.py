from django.apps import AppConfig


class AccountConfig(AppConfig):
    name = 'abc_app.account'
