from abc_app.product.models import Product
from abc_app.product.tasks import select_bookmarks_and_send_mails
from django.db import transaction
from django.db.models.signals import pre_save
from django.dispatch import receiver


@receiver(pre_save, sender=Product)
def product_update_handler(sender, instance, **kwargs):
    if instance.id:
        old_price = sender.objects.get(pk=instance.id).price
        if old_price > instance.price:
            transaction.on_commit(
                lambda: select_bookmarks_and_send_mails.delay(instance.id))
