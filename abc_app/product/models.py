from django.db import models
from django.contrib.postgres.search import SearchVectorField, SearchVector
from django.contrib.postgres.indexes import GinIndex
from mptt.models import MPTTModel, TreeForeignKey

from abc_app.supplier.models import Supplier
from abc_app.account.models import User


class Category(MPTTModel):
    name = models.CharField(max_length=100, verbose_name='Название')
    slug = models.SlugField(unique=True)
    parent = TreeForeignKey(
        'self',
        on_delete=models.CASCADE,
        related_name='subcategories',
        blank=True,
        null=True
    )

    class Meta:
        verbose_name = 'Категория'
        verbose_name_plural = 'Категории'

    def __str__(self):
        return self.name


class Brand(models.Model):
    name = models.CharField(max_length=60, verbose_name='Название')
    slug = models.SlugField(unique=True)
    code = models.IntegerField(verbose_name='Код')

    class Meta:
        verbose_name = 'Бренд'
        verbose_name_plural = 'Бренды'

    def __str__(self):
        return self.name


class ProductManager(models.Manager):
    def annotate_documents(self):
        vector = SearchVector('name', weight='A', config='russian') + \
            SearchVector('description', weight='B', config='russian')
        return self.get_queryset().annotate(document=vector)


class Product(models.Model):
    name = models.CharField(max_length=100, verbose_name='Название')
    slug = models.SlugField(unique=True)
    vendor_code = models.CharField(max_length=100, verbose_name='Артикул')
    description = models.CharField(max_length=2000, verbose_name='Описание')
    count = models.PositiveIntegerField(verbose_name='Количество в наличии')
    price = models.DecimalField(
        max_digits=10,
        decimal_places=2,
        verbose_name='Цена'
    )
    image = models.ImageField(default=None, blank=True)
    category = models.ForeignKey(
        Category,
        related_name='products',
        on_delete=models.CASCADE
    )
    supplier = models.ManyToManyField(Supplier)
    brand = models.ForeignKey(
        Brand,
        related_name='products',
        on_delete=models.CASCADE
    )
    search_vector = SearchVectorField(null=True, blank=True)

    objects = ProductManager()

    class Meta:
        verbose_name = 'Продукт'
        verbose_name_plural = 'Продукты'
        indexes = [
            GinIndex(fields=['search_vector'])
        ]

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        super().save(*args, **kwargs)
        if ('update_fields' not in kwargs or
                'search_vector' not in kwargs['update_fields']):
            instance = self._meta.default_manager.annotate_documents().\
                get(pk=self.pk)
            instance.search_vector = instance.document
            instance.save(update_fields=['search_vector'])


class Bookmark(models.Model):
    user = models.ForeignKey(
        User,
        on_delete=models.CASCADE,
        related_name='bookmarks'
    )
    product = models.ForeignKey(
        Product,
        on_delete=models.CASCADE,
        related_name='bookmarks'
    )

    class Meta:
        verbose_name = 'Избранное'
        verbose_name_plural = 'Избранные'

    def __str__(self):
        return '{0} {1}'.format(self.user, self.product)
