from django.contrib import admin
from .models import Brand, Category, Product, Bookmark

admin.site.register([Brand, Category, Product, Bookmark])
