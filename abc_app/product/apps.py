from django.apps import AppConfig


class ProductConfig(AppConfig):
    name = 'abc_app.product'

    def ready(self):
        import abc_app.product.signals
