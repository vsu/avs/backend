from django.core.mail import send_mail

from abc_app.product.models import Bookmark, Product
from settings.celery import app


@app.task
def send_email(title, body, sender, *emails):
    send_mail(title, body, sender, *emails, fail_silently=False)


@app.task
def select_bookmarks_and_send_mails(product_id):
    users_emails = Bookmark.objects.filter(product__id=product_id).values_list(
        'user__email', flat=False)
    if users_emails:
        product = Product.objects.get(pk=product_id)
        sender = 'yazubakhin@gmail.com'
        title = 'Цена "{}" изменилась'.format(str(product))
        body = 'Цена на товар "{}" снизилась. Новая цена {}.'.format(
            str(product), product.price)
        for email in users_emails:
            send_email.delay(title, body, sender, email)
