#!/usr/bin/env bash

docker login -u gitlab-ci-token -p ${CI_JOB_TOKEN} ${CI_REGISTRY}

docker tag avs_backend ${TAGGED_IMAGE}
docker tag avs_backend ${IMAGE}
docker push ${TAGGED_IMAGE}
docker push ${IMAGE}
