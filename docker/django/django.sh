#!/usr/bin/env bash

until python manage.py makemigrations --settings=settings.settings_docker
do
    echo 'Waiting for postgres ready'
done

python manage.py makemigrations product account supplier --settings=settings.settings_docker
python manage.py migrate --settings=settings.settings_docker
python manage.py collectstatic
gunicorn settings.wsgi -b 0.0.0.0:8001
