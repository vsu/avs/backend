import pytest
from rest_framework.test import APIClient

from abc_app.product.models import Category
from api.v1.tests.utils.factories import (AddressFactory, BrandFactory,
                                          ProductFactory, SupplierFactory,
                                          UserFactory)


@pytest.fixture
def user(db):
    user = UserFactory(email='citizen321@test.com')
    user.set_password('testpass123')
    user.save()
    return user


@pytest.fixture
def client(db, user):
    client = APIClient()
    client.force_authenticate(user=user)
    return client


@pytest.fixture
def brands(db):
    brand1 = BrandFactory(name='brand1', slug='brand1', code='123')
    brand2 = BrandFactory(name='brand2', slug='brand2', code='122')
    brand3 = BrandFactory(name='brand3', slug='brand3', code='124')
    return [brand1, brand2, brand3]


@pytest.fixture
def categories(db):
    category1 = Category.objects.create(name='category1', slug='cat1')
    category2 = Category.objects.create(
        name='category2', slug='cat2', parent=category1)
    category3 = Category.objects.create(name='category3', slug='cat3')
    return [category1, category2, category3]


@pytest.fixture
def suppliers(db):
    supplier1 = SupplierFactory(
        id=1,
        name='TestSupplier', slug='testsupplier1', code=64)
    supplier2 = SupplierFactory(
        id=2,
        name='TestSupplier2',
        slug='testsupplier2',
        code=128
    )
    supplier3 = SupplierFactory(
        id=3,
        name='TestSupplier3',
        slug='testsupplier3',
        code=256
    )
    address1 = AddressFactory(
        supplier=supplier1,
        city='DefaultCity',
        country='Russia',
        street='Rabotnitsa',
        building=4
    )
    address2 = AddressFactory(
        supplier=supplier1,
        city='DefaultCity',
        country='Russia',
        street='Karla Marksa',
        building=2
    )
    address3 = AddressFactory(
        supplier=supplier2,
        city='DefaultCity2',
        country='Russia',
        street='DefaultProspekt',
        building=24
    )
    address4 = AddressFactory(
        supplier=supplier3,
        city='DefaultCity2',
        country='Russia',
        street='20 Oktyabtya',
        building=65
    )
    return [supplier1, supplier2, supplier3]


@pytest.fixture
def products(db, brands, suppliers, categories):
    product1 = ProductFactory(
        name='product1',
        slug='pr1',
        category=categories[1],
        brand=brands[1],
        supplier=[suppliers[0]],
        vendor_code='1',
        description='sample',
        count=2,
        price=122,
    )
    product2 = ProductFactory(
        name='product2',
        slug='pr2',
        category=categories[2],
        brand=brands[2],
        supplier=[suppliers[1]],
        vendor_code='2',
        description='sample',
        count=22,
        price=133,
    )
    return [product1, product2]
