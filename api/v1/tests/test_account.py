import factory
import pytest
from django.urls import reverse
from rest_framework import status
from rest_framework.test import APIClient

from abc_app.account.models import User
from api.v1.tests.utils.factories import UserFactory


@pytest.fixture
def user(db):
    user = UserFactory(email='citizen321@test.com')
    user.set_password('testpass123')
    user.save()
    return user


@pytest.mark.django_db
def test_register_account_succefull():
    client = APIClient()
    url = reverse('register')
    data = {'email': 'citizen2@test.com', 'password': 'testpass123'}
    response = client.post(url, data, format='json')
    assert response.status_code == status.HTTP_200_OK


@pytest.mark.django_db
def test_register_account_password_too_short():
    client = APIClient()
    url = reverse('register')
    data = {'email': 'citizen2@test.com', 'password': '2short'}
    response = client.post(url, data, format='json')
    assert response.status_code == status.HTTP_400_BAD_REQUEST
    assert response.data['password'] == [
        'Ensure this field has at least 8 characters.']


@pytest.mark.django_db
def test_register_account_email_already_exists(user):
    client = APIClient()
    url = reverse('register')
    data = {'email': 'citizen2@test.com', 'password': 'testpass123'}
    response = client.post(url, data, format='json')
    assert response.status_code == status.HTTP_200_OK
    response = client.post(url, data, format='json')
    assert response.status_code == status.HTTP_400_BAD_REQUEST
    assert response.data['email'] == ['This field must be unique.']


@pytest.mark.django_db
def test_login_account_succeful(user):
    client = APIClient()
    url = reverse('login')
    data = {
        'email': 'citizen321@test.com',
        'password': 'testpass123'
    }
    response = client.post(url, data=data, format='json')
    assert response.status_code == status.HTTP_200_OK
    assert 'email' in response.data
    assert 'token' in response.data


def test_login_account_error(user):
    client = APIClient()
    url = reverse('login')
    data_wrong_email = {
        'email': 'citizen321@wrong.com',
        'password': 'testpass123'
    }
    data_wrong_pass = {
        'email': 'citizen321@test.com',
        'password': 'wrongpass123'
    }
    data_without_pass = {
        'email': 'citizen321@test.com'
    }
    data_without_email = {
        'password': 'testpass123'
    }
    response = client.post(url, data=data_wrong_email, format='json')
    assert response.status_code == status.HTTP_400_BAD_REQUEST
    response = client.post(url, data=data_wrong_pass, format='json')
    assert response.status_code == status.HTTP_400_BAD_REQUEST
    response = client.post(url, data=data_without_pass, format='json')
    assert response.status_code == status.HTTP_400_BAD_REQUEST
    response = client.post(url, data=data_without_email, format='json')
    assert response.status_code == status.HTTP_400_BAD_REQUEST


@pytest.mark.django_db
def test_logout_account_succeful(user):
    client = APIClient()
    url = reverse('logout')
    url_login = reverse('login')
    data = {
        'email': 'citizen321@test.com',
        'password': 'testpass123'
    }
    response = client.post(url_login, data=data, format='json')
    assert 'token' in response.data
    client.credentials(
        HTTP_AUTHORIZATION='Token {}'.format(response.data['token'])
    )
    response = client.post(url, data=response.data, format='json')
    assert response.status_code == status.HTTP_204_NO_CONTENT
