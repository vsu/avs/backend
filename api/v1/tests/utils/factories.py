import factory


from abc_app.product.models import Brand, Product, Category
from abc_app.supplier.models import Address, Supplier
from abc_app.account.models import User


class UserFactory(factory.DjangoModelFactory):
    class Meta:
        model = User
        django_get_or_create = ('email',)
    email = 'citizen@test.com'


class AddressFactory(factory.DjangoModelFactory):
    class Meta:
        model = Address


class SupplierFactory(factory.DjangoModelFactory):
    class Meta:
        model = Supplier
        django_get_or_create = ('name', 'slug', 'code')

    name = factory.Sequence(lambda n: 'Supplier {}'.format(n))


class BrandFactory(factory.DjangoModelFactory):
    class Meta:
        model = Brand
        django_get_or_create = ('name', 'slug', 'code')

    name = 'coolbrand'
    slug = 'coolbrand'
    code = '123'


class ProductFactory(factory.DjangoModelFactory):
    class Meta:
        model = Product

    @factory.post_generation
    def supplier(self, create, extracted, **kwargs):
        if not create:
            return
        if extracted:
            for supplier in extracted:
                self.supplier.add(supplier)
