import json

import pytest
from django.urls import reverse
from rest_framework import status

from abc_app.product.models import Category
from api.v1.tests.utils.factories import (BrandFactory, ProductFactory,
                                          SupplierFactory, UserFactory)

VALID_DATA = {
    'count': 3,
    'next': None,
    'previous': None,
    'results': [
        {
            "id": 1,
            "name": "brand1"
        },
        {
            "id": 2,
            "name": "brand2"
        },
        {
            "id": 3,
            "name": "brand3"
        }
    ]
}


VALID_DATA_CAT = {
    'count': 2,
    'next': None,
    'previous': None,
    'results': [
        {
            "id": 1,
            "name": "category1",
            "subcategories": [
                {
                    "id": 2,
                    "name": "category2",
                    "subcategories": []
                }
            ]
        },
        {
            "id": 3,
            "name": "category3",
            "subcategories": []
        }
    ]
}

VALID_DATA_CAT_DETAIL = [
    {
        "id": 4,
        "name": "category4",
        "subcategories": [
            {
                "id": 5,
                "name": "category5",
                "subcategories": []
            }
        ]
    }
]

VALID_DATA_PRODUCT_LIST = {
    'count': 2,
    'next': None,
    'previous': None,

    'results': [
        {
            'bookmarked': False,
            'brand': 5,
            'category': 8,
            'count': 2,
            'description': 'sample',
            'id': 1,
            'image': None,
            'name': 'product1',
            'price': '122.00',
            'supplier': [{'id': 1, 'name': 'TestSupplier'}],
            'vendor_code': '1'
        },
        {
            'bookmarked': False,
            'brand': 6,
            'category': 9,
            'count': 22,
            'description': 'sample',
            'id': 2,
            'image': None,
            'name': 'product2',
            'price': '133.00',
            'supplier': [{'id': 2, 'name': 'TestSupplier2'}],
            'vendor_code': '2'
        }
    ]
}


@pytest.mark.django_db
def test_category_list(client):
    category1 = Category.objects.create(name='category1', slug='cat1')
    category1.save()
    category2 = Category.objects.create(
        name='category2', slug='cat2', parent=category1)
    category2.save()
    category3 = Category.objects.create(name='category3', slug='cat3')
    category3.save()
    response = client.get('/api/v1/categories/')
    print('\nDATA', response.data)
    assert json.loads(response.content) == VALID_DATA_CAT


@pytest.mark.django_db
def test_brands_list(client, brands):
    url_brand = reverse('brands')
    response = client.get(url_brand, format='json')
    assert json.loads(response.content) == VALID_DATA


@pytest.mark.django_db
def test_category_detail(client):
    category4 = Category.objects.create(name='category4', slug='cat4')
    category5 = Category.objects.create(
        name='category5', slug='cat5', parent=category4)
    category6 = Category.objects.create(name='category6', slug='cat6')
    response = client.get(
        '/api/v1/categories/{}/'.format(category4.id),
        format='json'
    )
    assert json.loads(response.content) == VALID_DATA_CAT_DETAIL


@pytest.mark.django_db
def test_prouduct_list(client, products):
    response = client.get('/api/v1/products/', format='json')
    assert json.loads(response.content) == VALID_DATA_PRODUCT_LIST


@pytest.mark.django_db
def test_product_detail(client, products):
    obj = products[0]
    expected = {
        'bookmarked': False,
        'brand': obj.brand.id,
        'category': obj.category.id,
        'count': obj.count,
        'description': 'sample',
        'id': obj.id,
        'image': None,
        'name': 'product1',
        'price': '122.00',
        'supplier': [{'id': obj.supplier.first().id, 'name': 'TestSupplier'}],
        'vendor_code': '1'
    }
    response = client.get('/api/v1/products/{}/'.format(obj.id), format='json')
    assert json.loads(response.content) == expected


@pytest.mark.django_db
def test_product_bookmark(client, products):
    obj = products[0]
    response = client.post(
        '/api/v1/products/{}/bookmark/'.format(obj.id),
        format='json'
    )
    assert response.status_code == status.HTTP_200_OK
    response = client.get('/api/v1/products/{}/'.format(obj.id), format='json')
    data = json.loads(response.content)
    assert data['bookmarked']
    response = client.get('/api/v1/bookmarks/', format='json')
    expected = {
        'count': 1,
        'next': None,
        'previous': None,
        'results': [
            {
                'bookmarked': True,
                'brand': obj.brand.id,
                'category': obj.category.id,
                'count': obj.count,
                'description': 'sample',
                'id': obj.id,
                'image': None,
                'name': 'product1',
                'price': '122.00',
                'supplier': [
                    {'id': obj.supplier.first().id, 'name': 'TestSupplier'}],
                'vendor_code': '1'
            }
        ]
    }
    data = json.loads(response.content)
    assert data == expected
