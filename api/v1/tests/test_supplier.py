import json

import pytest
from django.urls import reverse
from rest_framework import status
from rest_framework.test import APIClient

from api.v1.tests.utils.factories import (AddressFactory, SupplierFactory,
                                          UserFactory)

pytestmark = pytest.mark.django_db

VALID_DATA = {
    'count': 3,
    'next': None,
    'previous': None,
    'results': [
        {
            "id": 1,
            "name": "TestSupplier",
            "addresses": [
                {"city": "DefaultCity", "country": "Russia",
                 "street": "Rabotnitsa", "building": "4"},
                {"city": "DefaultCity", "country": "Russia",
                 "street": "Karla Marksa", "building": "2"}
            ]
        },
        {
            "id": 2,
            "name": "TestSupplier2",
            "addresses": [
                {"city": "DefaultCity2", "country": "Russia",
                 "street": "DefaultProspekt", "building": "24"}
            ]
        },
        {
            "id": 3,
            "name": "TestSupplier3",
            "addresses": [
                {"city": "DefaultCity2", "country": "Russia",
                 "street": "20 Oktyabtya", "building": "65"}
            ]
        }
    ]
}


def test_supplier_list(client, suppliers):
    url = reverse('suppliers')
    response = client.get(url, format='json')
    assert json.loads(response.content) == VALID_DATA
