from django.conf.urls import url
from knox.views import LogoutView
from rest_framework import routers

from api.v1.account.views import UserLoginView, UserRegisterView,\
    UserResetPasswordView
from api.v1.product.views import BrandListView, CategoryDetailView,\
    CategoryListView, ProductViewSet, BookmarkListView
from api.v1.supplier.views import SupplierListView

router = routers.SimpleRouter()
router.register(r'products', ProductViewSet, base_name='products')

urlpatterns = [
    url(
        regex=r'^register/$',
        view=UserRegisterView.as_view(),
        name='register'
    ),
    url(
        regex=r'^login/$',
        view=UserLoginView.as_view(),
        name='login'
    ),
    url(
        regex=r'^reset_password/$',
        view=UserResetPasswordView.as_view(),
        name='reset'
    ),
    url(
        regex=r'^logout/$',
        view=LogoutView.as_view(),
        name='logout'
    ),
    url(
        regex=r'^categories/$',
        view=CategoryListView.as_view(),
        name='categories'
    ),
    url(
        regex=r'^categories/(?P<pk>\d+)/$',
        view=CategoryDetailView.as_view(),
        name='category'
    ),
    url(
        regex=r'^brands/$',
        view=BrandListView.as_view(),
        name='brands'
    ),
    url(
        regex=r'^suppliers/$',
        view=SupplierListView.as_view(),
        name='suppliers'
    ),
    url(
        regex=r'^bookmarks/$',
        view=BookmarkListView.as_view(),
        name='bookmarks'
    )
]

urlpatterns += router.urls
