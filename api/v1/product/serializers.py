from django.core.exceptions import ObjectDoesNotExist
from django.db.models import F, Prefetch
from rest_framework import serializers

from abc_app.product.models import Brand, Category, Product, Bookmark
from api.v1.supplier.serializers import SupplierSerializer


class BrandSerializer(serializers.ModelSerializer):
    class Meta:
        model = Brand
        fields = ('id', 'name',)


class CategorySerializer(serializers.ModelSerializer):
    subcategories = serializers.SerializerMethodField()

    class Meta:
        model = Category
        fields = ('id', 'name', 'subcategories')

    def get_subcategories(self, category):
        """Используем get_children внутри кэшированного дерева.
        Магия mptt работает.
        """
        queryset = category.get_children()
        serializer_data = CategorySerializer(
            queryset, many=True,
            read_only=True,
            context=self.context
        )
        return serializer_data.data


class ProductDetailSerializer(serializers.ModelSerializer):
    supplier = SupplierSerializer(many=True)
    bookmarked = serializers.SerializerMethodField(method_name='is_bookmarked')

    class Meta:
        model = Product
        fields = ('id', 'name', 'category', 'brand', 'supplier', 'vendor_code',
                  'description', 'count', 'price', 'image', 'bookmarked',)

    def is_bookmarked(self, obj):
        user = self.context['request'].user
        users = set(bookmark.user for bookmark in obj.bookmarks.all())
        if obj.bookmarks is not None and user in users:
            return True
        return False

    @staticmethod
    def eager_loading(queryset):
        queryset = queryset.select_related('brand', 'category')
        queryset = queryset.prefetch_related('supplier')
        queryset_bookmarks = Bookmark.objects.all().select_related('user')
        queryset = queryset.prefetch_related(
            Prefetch('bookmarks', queryset=queryset_bookmarks)
        )
        return queryset


class ProductSerializer(ProductDetailSerializer):
    description = serializers.SerializerMethodField()

    def get_description(self, obj):
        return obj.description[:30]


class CommaSeparatedIntegerField(serializers.Field):
    def to_representation(self, value):
        return value

    def to_internal_value(self, data):
        values = data.split(',')
        try:
            values = list(map(int, values))
        except ValueError:
            msg = 'Invalid type. Expected a integer.'
            raise serializers.ValidationError(msg)
        return values


class CommaSeparatedOrderField(serializers.Field):
    default_error_messages = {
        'invalid': '"{input}" is not a valid order field.'
    }

    VALUES = {
        'price',
        '-price'
    }

    def to_representation(self, value):
        return value

    def to_internal_value(self, data):
        values = data.split(',')
        for value in values:
            if value not in self.VALUES:
                self.fail('invalid', input=data)
        return values


class ProductQueryStringSerializer(serializers.Serializer):
    is_available = serializers.BooleanField()
    image = serializers.BooleanField()
    categories = CommaSeparatedIntegerField(required=False)
    brands = CommaSeparatedIntegerField(required=False)
    order = CommaSeparatedOrderField(required=False)
    q = serializers.CharField(required=False)

    def validate(self, attrs):
        brands = attrs.get('brands')
        categories = attrs.get('categories')
        if brands:
            for brand in brands:
                try:
                    Brand.objects.get(id=brand)
                except ObjectDoesNotExist:
                    msg = 'Brand with id={} does not exist'.format(brand)
                    raise serializers.ValidationError(msg)
            attrs['brands'] = brands
        if categories:
            flat_leafs = []
            for category in categories:
                try:
                    leafs = Category.objects.get(id=category)
                    # Если текущая категория -- лист, то добавляем её в список
                    if leafs.is_leaf_node():
                        flat_leafs.append(leafs.id)
                    # Иначе получаем список всех листьев этого узла
                    else:
                        flat_leafs.extend(leafs.get_descendants().
                                          filter(lft=F('rght') - 1).
                                          values_list('id', flat=True))
                except ObjectDoesNotExist:
                    msg = 'Category with id={} does not exist'.format(category)
                    raise serializers.ValidationError(msg)
            attrs['categories'] = list(set(flat_leafs))
        return attrs
