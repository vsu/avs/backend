from django.contrib.postgres.search import SearchQuery, SearchRank
from django.db.models import F
from knox.auth import TokenAuthentication
from mptt.templatetags.mptt_tags import cache_tree_children
from mptt.utils import get_cached_trees
from rest_framework import generics, mixins, status, viewsets
from rest_framework.decorators import detail_route
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework_extensions.cache.decorators import cache_response

from abc_app.product.models import Brand, Category, Product, Bookmark
from abc_app.account.models import User
from api.v1.product.serializers import BrandSerializer, CategorySerializer, \
    ProductDetailSerializer, ProductQueryStringSerializer, ProductSerializer
from api.v1.utils.utils import cache_for, calculate_cache_key


class CategoryListView(generics.ListAPIView):
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)
    serializer_class = CategorySerializer
    # Возвращает queryset root nodes.
    # Магия mptt работает, если использовать get_children()
    # в queryset из get_cached_trees
    # queryset = get_cached_trees(Category.objects.all())

    def get_queryset(self):
        queryset = get_cached_trees(Category.objects.all())
        return queryset


class CategoryDetailView(generics.GenericAPIView, mixins.RetrieveModelMixin):
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)
    serializer_class = CategorySerializer

    def get(self, request, pk):
        instance = cache_tree_children(Category.objects.get(
            pk=pk).get_descendants(include_self=True))
        serializer = self.get_serializer(instance, many=True)
        return Response(serializer.data)

    def get_queryset(self):
        queryset = get_cached_trees(Category.objects.all())
        return queryset


class BrandListView(generics.ListAPIView):
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)
    queryset = Brand.objects.all()
    serializer_class = BrandSerializer


class ProductViewSet(viewsets.ModelViewSet):
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)
    serializer_class = ProductDetailSerializer
    serializer_list_class = ProductSerializer

    @cache_for(7200, 'products')
    def get_queryset(self):
        queryset = Product.objects.all()
        queryset = self.get_serializer_class().eager_loading(queryset)
        return queryset

    def get_serializer_list(self, *args, **kwargs):
        if hasattr(self, 'serializer_list_class'):
            serializer_class = self.serializer_list_class
        else:
            return self.get_serializer(*args, **kwargs)
        kwargs['context'] = self.get_serializer_context()
        return serializer_class(*args, **kwargs)

    @cache_for(7200, 'filtered_products')
    def filter(self, queryset, query_params):
        # Поиск
        search_q = query_params.get('q')
        if search_q:
            query = SearchQuery(search_q, config='russian')
            queryset = queryset.annotate(
                rank=SearchRank(
                    F('search_vector'), query
                )
            ).filter(search_vector=query).order_by('-rank')
        # Фильтрация
        # По наличию изображения
        image = query_params.get('image')
        if image:
            queryset = queryset.exclude(image__exact='')
        # По доступности
        is_available = query_params.get('is_available')
        if is_available:
            queryset = queryset.filter(count__gt=0)
        # По категориям
        categories = query_params.get('categories')
        if categories:
            queryset = queryset.filter(category_id__in=categories)
        # По бренду
        brands = query_params.get('brands')
        if brands:
            queryset = queryset.filter(
                brand_id__in=brands)
        # Сортировка
        order = query_params.get('order')
        if order:
            queryset = queryset.order_by(*order)
        return queryset

    @cache_response(key_func=calculate_cache_key)
    def list(self, request):
        queryset = self.get_queryset()
        # Если нет никаких параметров фильтрации/поиска
        if not request.query_params:
            page = self.paginate_queryset(queryset)
            if page:
                serializer = self.get_serializer_list(page, many=True)
                return self.get_paginated_response(serializer.data)
            serializer = self.get_serializer_list(queryset, many=True)
            return Response(serializer.data)

        query_serializer = ProductQueryStringSerializer(
            data=request.query_params
        )
        query_serializer.is_valid(raise_exception=True)
        queryset = self.filter(queryset, query_serializer.data)
        page = self.paginate_queryset(queryset)
        if page:
            serializer = self.get_serializer_list(page, many=True)
            return self.get_paginated_response(serializer.data)
        serializer = self.get_serializer_list(queryset, many=True)
        return Response(serializer.data)

    @detail_route(methods=['post'])
    def bookmark(self, request, pk=None):
        user = request.user
        product = self.get_object()
        bookmark, created = Bookmark.objects.get_or_create(
            user=user, product=product
        )
        if not created:
            bookmark.delete()
            return Response(data=None, status=status.HTTP_204_NO_CONTENT)
        data = {
            'detail': 'Product with id={} bookmarked.'.format(product.id)
        }
        return Response(data, status=status.HTTP_200_OK)


class BookmarkListView(generics.ListAPIView):
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)
    serializer_class = ProductSerializer

    def get_queryset(self):
        queryset = Product.objects.all()
        queryset = self.get_serializer_class().eager_loading(queryset)
        return queryset

    def list(self, request, *args, **kwargs):
        user = request.user
        queryset = self.get_queryset().filter(bookmarks__user=user)
        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)
