from django.shortcuts import get_object_or_404
from rest_framework import serializers
from rest_framework.authentication import authenticate
from rest_framework.exceptions import NotFound
from rest_framework.validators import UniqueValidator

from abc_app.account.models import User


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('email',)


class AuthTokenSerializer(serializers.Serializer):
    email = serializers.EmailField(required=True)
    password = serializers.CharField(min_length=8, trim_whitespace=False)

    def validate(self, attrs):
        email = attrs.get('email')
        password = attrs.get('password')
        user = authenticate(
            self.context.get('request'),
            username=email,
            password=password
        )
        if not user:
            msg = 'Incorrect authorization credentials'
            raise serializers.ValidationError(msg, code='authorization')
        if not user.is_active:
            msg = 'User account is not active'
            raise serializers.ValidationError(
                msg, code='authorization')
        attrs['user'] = user
        return attrs


class UserRegisterSerializer(serializers.ModelSerializer):
    email = serializers.EmailField(
        required=True,
        validators=[UniqueValidator(queryset=User.objects.all())]
    )
    password = serializers.CharField(min_length=8, write_only=True)

    class Meta:
        model = User
        fields = ('email', 'password')

    def create(self, validated_data):
        user = User.objects.create_user(
            validated_data['email'],
            validated_data['password']
        )
        user.save()
        return user


class UserResetPasswordSerializer(serializers.Serializer):
    email = serializers.EmailField(
        required=True
    )

    def validate(self, attrs):
        email = attrs.get('email')
        user = get_object_or_404(User, email=email)
        new_password = User.objects.make_random_password()
        user.set_password(new_password)
        user.save()
        return attrs
