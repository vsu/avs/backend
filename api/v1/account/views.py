from knox.models import AuthToken
from rest_framework import status
from rest_framework.generics import GenericAPIView
from rest_framework.response import Response

from abc_app.account.tasks import send_new_password
from api.v1.account.serializers import AuthTokenSerializer, \
    UserRegisterSerializer, UserResetPasswordSerializer


class UserRegisterView(GenericAPIView):
    serializer_class = UserRegisterSerializer
    authentication_classes = ()

    def create(self, request):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        user = serializer.save()
        token = AuthToken.objects.create(user)
        data = {
            'email': user.email,
            'token': token
        }
        return Response(data, status=status.HTTP_200_OK)

    def perform_create(self, serializer):
        serializer.save()

    def post(self, request):
        """
        Создаёт нового пользователя.

        Возвращает email и token, если данные валидны.
        В противном случае возвращает сообщение об ошибке.
        Статус ошибки HTTP 400.
        """
        return self.create(request)


class UserLoginView(GenericAPIView):
    serializer_class = AuthTokenSerializer
    authentication_classes = ()
    permission_classes = ()

    def post(self, request):
        """
        Возвращает token и email, если данные валидны.
        В противном случае возвращает сообщение об ошибке.
        Статус ошибки HTTP 400.
        """
        serializer = self.serializer_class(
            data=request.data, context={'request': request})
        serializer.is_valid(raise_exception=True)
        user = serializer.validated_data['user']
        token = AuthToken.objects.create(user=user)
        data = {
            'email': user.email,
            'token': token
        }
        return Response(data=data, status=status.HTTP_200_OK)


class UserResetPasswordView(GenericAPIView):
    serializer_class = UserResetPasswordSerializer

    def post(self, request):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        send_new_password.delay(serializer.validated_data.get('email'))
        data = {
            'detail': 'New password sent to email.'
        }
        return Response(data)
