import hashlib
from django.core.cache import cache


def cache_get_key(*args):
    serialize = []
    for arg in args:
        serialize.append(str(arg))
    key = hashlib.md5(''.join(serialize).encode(encoding='utf_8')).hexdigest()
    return key


def calculate_cache_key(view_instance, view_method,
                        request, args, kwargs):
    serialize = []
    for arg in args:
        serialize.append(str(arg))
    serialize.append(str(request.query_params))
    key = hashlib.md5(
        ''.join(serialize).encode(encoding='utf_8')
    ).hexdigest()
    return key


def cache_for(time, key_name):
    def decorator(fn):
        def wrapper(self, *args, **kwargs):
            key = cache_get_key(fn.__name__, key_name, *args)
            result = cache.get(key)
            if not result:
                result = fn(self, *args, **kwargs)
                cache.set(key, result, time)
            return result
        return wrapper
    return decorator
