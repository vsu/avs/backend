from knox.auth import TokenAuthentication
from rest_framework import generics
from rest_framework.permissions import IsAuthenticated

from abc_app.supplier.models import Supplier
from api.v1.supplier.serializers import SupplierDetailSerializer


class SupplierListView(generics.ListAPIView):
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)
    serializer_class = SupplierDetailSerializer
    queryset = Supplier.objects.all().prefetch_related('addresses')
