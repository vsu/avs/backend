from rest_framework import serializers

from abc_app.supplier.models import Supplier, Address


class AddressSerializer(serializers.ModelSerializer):
    class Meta:
        model = Address
        fields = ('city', 'country', 'street', 'building')


class SupplierSerializer(serializers.ModelSerializer):
    class Meta:
        model = Supplier
        fields = ('id', 'name',)


class SupplierDetailSerializer(SupplierSerializer):
    addresses = AddressSerializer(many=True, read_only=True)

    class Meta(SupplierSerializer.Meta):
        fields = ('id', 'name', 'addresses',)
