from settings.settings import *

ALLOWED_HOSTS = ['web']
DEBUG = False
STATIC_ROOT = '/static/'
MEDIA_ROOT = '/media/'

CACHES = {
    "default": {
        "BACKEND": "django_redis.cache.RedisCache",
        "LOCATION": "redis://redis:6379/0",
        "OPTIONS": {
            "CLIENT_CLASS": "django_redis.client.DefaultClient",
        }
    }
}

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'postgres',
        'USER': 'postgres',
        'HOST': 'db',
        'PORT': '5432',
    }
}

CELERY_RESULT_BACKEND = 'redis://redis:6379/0'
BROKER_URL = 'redis://redis:6379/0'
